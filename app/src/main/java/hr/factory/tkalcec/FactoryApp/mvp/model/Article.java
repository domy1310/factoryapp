package hr.factory.tkalcec.FactoryApp.mvp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.realm.RealmObject;

public class Article extends RealmObject implements Parcelable {
    private String author;
    private String title;
    private String description;
    private String url;
    private String urlToImage;
    private Date publishedAt;
    private Date timestamp;

    @Inject
    public Article() {
    }

    public Article(Parcel in) {
        author = in.readString();
        title = in.readString();
        description = in.readString();
        url = in.readString();
        urlToImage = in.readString();
    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    public void setUrlToImage(String urlToImage) {
        this.urlToImage = urlToImage;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(Date publishedAt) {
        this.publishedAt = publishedAt;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public List<Article> articleMapper(ArticlesResponse articlesResponse) {
        List<Article> articleList = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        Date currentTime = calendar.getTime();

        if (articlesResponse != null) {

            final List<Article> articles = articlesResponse.getArticles();
            if (articles != null) {
                for(Article article : articles) {
                    Article newArticle = new Article();

                    newArticle.setAuthor(article.getAuthor());
                    newArticle.setDescription(article.getDescription());
                    newArticle.setPublishedAt(article.getPublishedAt());
                    newArticle.setTitle(article.getTitle());
                    newArticle.setUrl(article.getUrl());
                    newArticle.setUrlToImage(article.getUrlToImage());
                    newArticle.setTimestamp(currentTime);

                    articleList.add(newArticle);
                }
            }
        }

        return articleList;
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(author);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(url);
        dest.writeString(urlToImage);
    }
}
