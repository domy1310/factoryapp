package hr.factory.tkalcec.FactoryApp.mvp.view.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import hr.factory.tkalcec.FactoryApp.R;
import hr.factory.tkalcec.FactoryApp.mvp.model.Article;

public class ArticleAdapter extends RecyclerView.Adapter <ArticleAdapter.ViewHolder> {
    private static final String TAG = "ArticleAdapter";

    private List<Article> articleList = new ArrayList<>();
    private Context context;
    private OnArticleListClickListener mOnArticleListClickListener;

    public ArticleAdapter(Context context, OnArticleListClickListener onArticleListClickListener) {
        this.context = context;
        this.mOnArticleListClickListener = onArticleListClickListener;
    }

    @NonNull
    @Override
    public ArticleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View articleView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news, viewGroup, false);
        return new ViewHolder(articleView, mOnArticleListClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleAdapter.ViewHolder viewHolder, int i) {
        try {
            Article article = this.articleList.get(i);
            viewHolder.tvItemTitle.setText(article.getTitle());
            viewHolder.tvItemDesc.setText(article.getDescription());
            Glide.with(context).load(article.getUrlToImage())
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .into(viewHolder.ivArticleImage);
        } catch (NullPointerException e) {
            Log.e(TAG, "onBindViewHolder: Null pointer: " + e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return this.articleList.size();
    }

    public void addArticles(List<Article> articleList){
        this.articleList.addAll(articleList);
        notifyDataSetChanged();
    }

    public List<Article> getArticles(){
        return this.articleList;
    }

    public void clearArticles() {
        this.articleList.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivArticleImage;
        private TextView tvItemTitle;
        private TextView tvItemDesc;

        public OnArticleListClickListener mOnArticleListClickListener;

        public ViewHolder(@NonNull View itemView, OnArticleListClickListener onArticleListClickListener) {
            super(itemView);

            ivArticleImage = itemView.findViewById(R.id.ivArticleImage);
            tvItemTitle = itemView.findViewById(R.id.tvItemTitle);
            tvItemDesc = itemView.findViewById(R.id.tvItemDesc);

            mOnArticleListClickListener = onArticleListClickListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnArticleListClickListener.onArticleListClickListener(getAdapterPosition());
        }
    }

    public interface OnArticleListClickListener {
        void onArticleListClickListener(int position);
    }

}

