package hr.factory.tkalcec.FactoryApp.mvp.presenter;

import java.util.List;

import javax.inject.Inject;

import hr.factory.tkalcec.FactoryApp.mvp.model.RealmStorage;
import hr.factory.tkalcec.FactoryApp.api.ArticlesApiService;
import hr.factory.tkalcec.FactoryApp.mvp.model.Article;
import hr.factory.tkalcec.FactoryApp.mvp.model.ArticlesResponse;
import hr.factory.tkalcec.FactoryApp.mvp.view.main.MainView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class ArticlePresenter extends BasePresenter<MainView> implements Observer<ArticlesResponse>, RealmStorage.OnTransactionCallback {

    @Inject protected ArticlesApiService articlesApiService;
    @Inject protected RealmStorage realmStorage;
    @Inject protected Article article;


    @Inject
    public ArticlePresenter() {
    }

    public void getArticles() {
        getView().onShowProgressBar();
        if (realmStorage.isRealmEmpty()) {
            Observable<ArticlesResponse> articles = articlesApiService.getArticles();
            subscribe(articles, this);
        }
        else {
            if (realmStorage.isArticlesOld()) {
                realmStorage.deleteAllArticles();
                Observable<ArticlesResponse> articles = articlesApiService.getArticles();
                subscribe(articles, this);
            }
            else {
                getView().onArticleLoaded(realmStorage.readAllFromRealm());
                getView().onHideProgressBar();
            }
        }
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(ArticlesResponse articlesResponse) {
        List<Article> articles = article.articleMapper(articlesResponse);

        realmStorage.writeToRealm(articles, this);
    }

    @Override
    public void onError(Throwable e) {
        getView().onHideProgressBar();
        getView().onShowToast("Error loading articles " + e.getMessage());
    }

    @Override
    public void onComplete() {
        getView().onHideProgressBar();
        getView().onShowToast("Articles loading completed!");
    }

    @Override
    public void onRealmSuccess() {
        getView().onArticleLoaded(realmStorage.readAllFromRealm());
    }

    @Override
    public void onRealmError(Throwable t) {
        getView().onHideProgressBar();
        getView().onShowToast("Articles saving error!");
    }
}
