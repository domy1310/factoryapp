package hr.factory.tkalcec.FactoryApp.utils;

public class Constants {

    public static final String BASE_URL = "https://newsapi.org/";
    public static final String NEWS_API = "/v1/articles?source=bbc-news&sortBy=top&apiKey=6946d0c07a1c4555a4186bfcade76398";
}
