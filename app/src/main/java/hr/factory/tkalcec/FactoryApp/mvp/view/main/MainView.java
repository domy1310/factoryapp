package hr.factory.tkalcec.FactoryApp.mvp.view.main;

import java.util.List;

import hr.factory.tkalcec.FactoryApp.mvp.model.Article;
import hr.factory.tkalcec.FactoryApp.mvp.view.base.BaseView;

public interface MainView extends BaseView {
    void onArticleLoaded(List<Article> articleList);

    void onHideProgressBar();

    void onShowProgressBar();

    void onShowToast(String s);
}
