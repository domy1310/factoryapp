package hr.factory.tkalcec.FactoryApp.mvp.view.base;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import hr.factory.tkalcec.FactoryApp.application.FactoryNewsApp;
import hr.factory.tkalcec.FactoryApp.deps.component.AppComponent;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        onViewReady(savedInstanceState, getIntent());
    }

    @CallSuper
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        resolveDaggerDependency();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected AppComponent getAppComponent() {
        return ((FactoryNewsApp) getApplication()).getAppComponent();
    }

    protected void resolveDaggerDependency() {}

    protected abstract int getContentView();
}

