package hr.factory.tkalcec.FactoryApp.mvp.presenter;

import javax.inject.Inject;

import hr.factory.tkalcec.FactoryApp.mvp.view.base.BaseView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class BasePresenter<V extends BaseView> {

    @Inject protected V mainView;

    protected V getView() {
        return mainView;
    }

    protected <T> void subscribe(Observable<T> observable, Observer<T> observer) {
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}
