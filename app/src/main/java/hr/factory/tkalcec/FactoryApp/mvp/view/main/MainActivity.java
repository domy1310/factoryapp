package hr.factory.tkalcec.FactoryApp.mvp.view.main;


import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import hr.factory.tkalcec.FactoryApp.mvp.view.base.BaseActivity;
import hr.factory.tkalcec.FactoryApp.mvp.view.details.DetailsActivity;
import hr.factory.tkalcec.FactoryApp.mvp.view.main.adapter.ArticleAdapter;
import hr.factory.tkalcec.FactoryApp.R;
import hr.factory.tkalcec.FactoryApp.deps.component.DaggerArticleComponent;
import hr.factory.tkalcec.FactoryApp.deps.module.ArticleModule;
import hr.factory.tkalcec.FactoryApp.mvp.model.Article;
import hr.factory.tkalcec.FactoryApp.mvp.presenter.ArticlePresenter;

public class MainActivity extends BaseActivity implements MainView, ArticleAdapter.OnArticleListClickListener {

    protected RecyclerView rvArticleView;
    protected ProgressBar loadProgress;

    @Inject protected ArticlePresenter articlePresenter;
    private ArticleAdapter articleAdapter;

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        setUpView();
        articlePresenter.getArticles();
    }

    private void setUpView() {
        rvArticleView = (RecyclerView) findViewById(R.id.rvArticlesView);
        loadProgress = (ProgressBar) findViewById(R.id.loadProgress);

        rvArticleView.setHasFixedSize(true);
        rvArticleView.setLayoutManager(new LinearLayoutManager(this));
        rvArticleView.addItemDecoration(new DividerItemDecoration(this, 0 ));
        articleAdapter = new ArticleAdapter(this, this);
        rvArticleView.setAdapter(articleAdapter);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void resolveDaggerDependency() {
        DaggerArticleComponent.builder()
                .appComponent(getAppComponent())
                .articleModule(new ArticleModule(this))
                .build().inject(this);
    }

    @Override
    public void onArticleLoaded(List<Article> articleList) {
        articleAdapter.addArticles(articleList);
    }

    @Override
    public void onHideProgressBar() {
        loadProgress.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onShowProgressBar() {
        loadProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onShowToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onArticleListClickListener(int position) {
        Intent detailsIntent = new Intent(MainActivity.this, DetailsActivity.class);
        detailsIntent.putParcelableArrayListExtra(DetailsActivity.ARTICLES, (ArrayList<? extends Parcelable>) articleAdapter.getArticles());
        detailsIntent.putExtra(DetailsActivity.ARTICLE_POSITION, position);
        startActivity(detailsIntent);
    }
}
