package hr.factory.tkalcec.FactoryApp.api;

import hr.factory.tkalcec.FactoryApp.mvp.model.ArticlesResponse;
import hr.factory.tkalcec.FactoryApp.utils.Constants;
import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ArticlesApiService {

    @GET(Constants.NEWS_API)
    Observable<ArticlesResponse> getArticles();
}
