package hr.factory.tkalcec.FactoryApp.mvp.model;

import android.content.Context;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;

public class RealmStorage {
    private Realm realm;
    private Context context;

    @Inject
    public RealmStorage(Context context) {
        Realm.init(context);
        realm = Realm.getDefaultInstance();
        this.context = context;
    }

    public void writeToRealm(final List<Article> articles, final OnTransactionCallback onTransactionCallback) {
        realm.executeTransactionAsync(realm -> realm.insertOrUpdate(articles), () -> onTransactionCallback.onRealmSuccess(), error -> onTransactionCallback.onRealmError(error));
    }

    public RealmResults<Article> readAllFromRealm() {
        return realm.where(Article.class).findAll();
    }

    public void deleteAllArticles() {
        final RealmResults<Article> results = realm.where(Article.class).findAll();

        realm.executeTransaction(realm -> results.deleteAllFromRealm());
    }

    public boolean isRealmEmpty() {
        final RealmResults<Article> realmResults = realm.where(Article.class).findAll();

        if (realmResults.isEmpty()) {
            return true;
        }

        return false;
    }

    public boolean isArticlesOld() {
        Calendar calendar = Calendar.getInstance();
        Date timeNow = calendar.getTime();
        calendar.add(Calendar.MINUTE, -5);
        Date timeBeforeFiveMinutes = calendar.getTime();

        RealmResults<Article> articleRealmResults = realm.where(Article.class).between("timestamp", timeBeforeFiveMinutes, timeNow).findAll();

        if (articleRealmResults.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public interface OnTransactionCallback {
        void onRealmSuccess();
        void onRealmError(final Throwable t);
    }
}
