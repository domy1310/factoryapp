package hr.factory.tkalcec.FactoryApp.mvp.view.details.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import java.util.List;

import hr.factory.tkalcec.FactoryApp.R;
import hr.factory.tkalcec.FactoryApp.mvp.model.Article;

public class PageAdapter extends PagerAdapter {
    Context context;
    List<Article> articleList;

    public PageAdapter(Context context, List<Article> articleList) {
        this.context = context;
        this.articleList = articleList;
    }

    @Override
    public int getCount() {
        return articleList.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.item_single_article, container, false);

        ImageView ivSingleArticle;
        TextView tvSingleArticleTitle;
        TextView tvSingleArticleDesc;

        ivSingleArticle = (ImageView) view.findViewById(R.id.ivSingleArticle);
        tvSingleArticleTitle = (TextView) view.findViewById(R.id.tvSingleArticleTitle);
        tvSingleArticleDesc = (TextView) view.findViewById(R.id.tvSingleArticleDesc);

        Article article = articleList.get(position);

        tvSingleArticleTitle.setText(article.getTitle());
        tvSingleArticleDesc.setText(article.getDescription());
        Glide.with(context).load(article.getUrlToImage()).into(ivSingleArticle);

        container.addView(view);
        return view;
    }
}

