package hr.factory.tkalcec.FactoryApp.mvp.view.details;

import android.content.Intent;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import java.util.List;

import hr.factory.tkalcec.FactoryApp.mvp.view.base.BaseActivity;
import hr.factory.tkalcec.FactoryApp.R;
import hr.factory.tkalcec.FactoryApp.mvp.model.Article;
import hr.factory.tkalcec.FactoryApp.mvp.view.details.adapter.PageAdapter;

public class DetailsActivity extends BaseActivity implements DetailsView, ViewPager.OnPageChangeListener {

    public static final String ARTICLES = "articles";
    public static final String ARTICLE_POSITION = "position";

    private ViewPager vpArticleList;
    private List<Article> articleList;
    public int currentPosition;

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);

        vpArticleList = findViewById(R.id.vpArticles);
        //vpArticleList.setAdapter(new PagerAdapter(this, listDate);
        getIncomingArticles();

    }

    @Override
    public void getIncomingArticles() {
        if(getIntent().hasExtra(ARTICLES)) {
            articleList = (getIntent().getParcelableArrayListExtra(ARTICLES));
            currentPosition = getIntent().getIntExtra(ARTICLE_POSITION, 0);

            changeActivityTitle(currentPosition);
            setUpViewPager();
        }
    }

    private void setUpViewPager() {
        vpArticleList.setAdapter(new PageAdapter(this, articleList));
        vpArticleList.setCurrentItem(currentPosition);
        vpArticleList.addOnPageChangeListener(this);
    }

    @Override
    public void changeActivityTitle(int position) {
        setTitle(articleList.get(position).getTitle());
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_details;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

    @Override
    public void onPageSelected(int position) {
        changeActivityTitle(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
