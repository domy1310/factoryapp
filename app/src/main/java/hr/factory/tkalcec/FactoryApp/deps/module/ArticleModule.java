package hr.factory.tkalcec.FactoryApp.deps.module;

import dagger.Module;
import dagger.Provides;
import hr.factory.tkalcec.FactoryApp.deps.scope.PerActivity;
import hr.factory.tkalcec.FactoryApp.api.ArticlesApiService;
import hr.factory.tkalcec.FactoryApp.mvp.view.main.MainView;
import retrofit2.Retrofit;


@Module
public class ArticleModule {
    private MainView mainView;

    public ArticleModule(MainView mainView) {
        this.mainView = mainView;
    }

    @PerActivity
    @Provides
    ArticlesApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ArticlesApiService.class);
    }

    @PerActivity
    @Provides
    MainView provideView() {
        return mainView;
    }
}
