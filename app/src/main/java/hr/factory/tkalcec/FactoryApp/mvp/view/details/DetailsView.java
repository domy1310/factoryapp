package hr.factory.tkalcec.FactoryApp.mvp.view.details;

import hr.factory.tkalcec.FactoryApp.mvp.view.base.BaseView;

public interface DetailsView extends BaseView {
    void getIncomingArticles();

    void changeActivityTitle(int position);
}
