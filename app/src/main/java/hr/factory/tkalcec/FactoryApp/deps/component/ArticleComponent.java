package hr.factory.tkalcec.FactoryApp.deps.component;

import dagger.Component;
import hr.factory.tkalcec.FactoryApp.deps.module.ArticleModule;
import hr.factory.tkalcec.FactoryApp.deps.scope.PerActivity;
import hr.factory.tkalcec.FactoryApp.mvp.view.main.MainActivity;

@PerActivity
@Component(modules = ArticleModule.class, dependencies = AppComponent.class)
public interface ArticleComponent {
    void inject(MainActivity mainActivity);
}
