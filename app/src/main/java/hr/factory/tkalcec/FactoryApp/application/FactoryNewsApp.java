package hr.factory.tkalcec.FactoryApp.application;

import android.app.Application;

import hr.factory.tkalcec.FactoryApp.deps.component.AppComponent;
import hr.factory.tkalcec.FactoryApp.deps.component.DaggerAppComponent;
import hr.factory.tkalcec.FactoryApp.deps.module.AppModule;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class FactoryNewsApp extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initRealmConfiguration();
        initAppComponent();
    }

    private void initRealmConfiguration() {
        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(configuration);
    }

    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
