package hr.factory.tkalcec.FactoryApp.deps.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import hr.factory.tkalcec.FactoryApp.deps.module.AppModule;
import retrofit2.Retrofit;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    Retrofit exposeRetrofit();

    Context exposeContext();
}
